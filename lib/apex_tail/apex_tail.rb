# require https://github.com/ejholmes/restforce
#
# Example
# ruby apex_tail.rb | grep KEYWORD  # print only lines including KEYWORD
#
require 'restforce'
require 'yaml'

#require 'pp'
#require 'pry-byebug'

module ApexTail

  module Initializer
    ConfigPath = File.expand_path '~/.apex_tail.yaml'
    Default_Api_Version = '39.0'
    DefaultSleepSec = 5

    def setup(argv)
      result = {
        account: nil,
        sleeptime: DefaultSleepSec,
        list_username: false
      }
      configs = read_project_configures

      params = parse_args argv

      if params.has_key?(:list_username)
        result[:list_username] = true
        return result
      end

      if params.has_key?(:username)
        if configs.has_key?(params[:username])
          result[:account] = configs[params[:username]]
        else
          raise "project is not found."
        end
        result[:logurl] = params[:logurl]
      else
        config = ask_configure
        configs[config[:username]] = config
        result[:account] = configs[config[:username]]
        write_project_configres(configs)
      end

      return result
    end

    private
    def parse_args(argv)
      params = {}
      argstr = argv.join(' ')

      # list_usernameの場合、他のパラメータは見ない
      if argstr =~ /\s*--\blist_username/
        params[:list_username] = true
        return params
      end

      if argstr =~ /\s*--sleeptime\s+(\d+)\s*/
        params[:sleep_time] = $1.to_i
      end

      if argstr =~ /\s*--username\s+([-\w.+]+@[-\w.]+)\s*/
        params[:username] = $1
      end

      # logurlがある場合は、１回のみ取得する
      if argstr =~ /\s*--url\s+([^\s]+)/
        params[:logurl] = $1
      end

      return params
    end

    def read_project_configures(path = ConfigPath)
      if File.readable?(path)
        yaml = YAML.load(File.read(path))
        return yaml
      else
        return {}
      end
    end

    def write_project_configres(configs, path = ConfigPath)
      File.open(path, "w") do |f|
        f.write configs.to_yaml
      end
    end

    def ask_configure()
      config = {
        username: "",
        password: "",
        security_token: "",
        client_id: "",
        client_secret: "",
        api_version: Default_Api_Version
      }

      config.keys.each do |key|
        message = "#{key} ?"
        line = read_line(message)
        config[key] = line
      end

      return config
    end

    def read_line(message)
      line = ""
      while true
        print message
        line = STDIN.gets().strip()
        if not line.empty?
          break
        end
      end
      return line
    end


  end

end


################################################################################
# utilities

def get_now_time
  return Time.now.utc.to_datetime.rfc3339
end

################################################################################
# main
#
include ApexTail::Initializer

config = setup(ARGV)

if config[:list_username]
  projects = read_project_configures()
  projects.each do |proj|
    puts proj[0]
  end
  exit
end

client = Restforce.tooling(config[:account])
nowtime_utc = get_now_time()
sleeptime_sec = config[:sleeptime]
no_logfile_count_max = 5
no_logfile_count = no_logfile_count_max

# one shot
if config[:logurl]
  log = client.get(config[:logurl])
  puts log.body
  exit 
end

# cyclic
while true
  #puts nowtime_utc
  
  log_files = client.query("select Id, LogUserId, Operation, Status, StartTime from ApexLog where StartTime >= #{nowtime_utc} order by StartTime")
  nowtime_utc = get_now_time()

  if log_files.size == 0 && no_logfile_count > 0
    no_logfile_count -= 1
    if no_logfile_count == 0
      STDERR.puts "disconnected"
      STDERR.flush
    end
  elsif log_files.size != 0
    if no_logfile_count == 0
      STDERR.puts "connected"
      STDERR.flush
    end
    no_logfile_count = no_logfile_count_max
  end

  log_files.each { |log_file|
    url = "#{log_file['attributes']["url"]}/Body"
    if not (log_file['Status'] == '成功' || log_file['Status'].downcase == 'success')
      STDERR.puts "#{log_file['StartTime']}, #{log_file['Operation']}, #{log_file['Status']}, #{url}"
    end
    log = client.get(url)
    puts log.body
  }

  # sleepするとSTDOUTに書き出されないようなので、強制的に書き出している。
  STDOUT.flush

  sleep sleeptime_sec
end
