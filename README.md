# 概要

salesforceの開発者コンソールに表示されるログを、コンソールに定周期出力する。

# 必要なもの

- ruby 2.2.5 以降
- rubygems（インストールされていない場合) 

  コンソールで *gem --version* と入力して、バージョンが表示されない場合は、rubygemsのインストールが必要です。
  インストールはこちらから、https://rubygems.org/ 

  ※ 現在のところWindowsでは動作未確認です。

# インストール

ここからgemファイルをダウンロードしてください。

https://bitbucket.org/hmu/apex_tail/raw/20d322486f49e650ba3d861ad4e91938e82fe718/pkg/apex_tail-0.1.0.gem

コンソールから以下を実行でインストールされます。

> \> gem install apex_tail-0.1.0.gem

# salesforce側での準備

- セキュリティトークンの発行
- 接続アプリケーションの設定(Oauthを利用するため、client-id client-secretが必要）

# 使い方

### salesforceとの初回接続時

> \> apex_tail 

※username, password, securitytoken, client-id, client-secretの入力が求められるので、それに従って入力します。

### 登録済みのsalesforceへ接続する場合

> \> apex_tail --username *username@email.com*

### 単発apexlogのurlからログを取得する場合

> \> apex_tail --username *username@email.com* --url */services/~~~~*


##　注意事項

- salesforceからのログ取得のため、５秒周期でAPIコール数を消費します。
- 一度開発者コンソールを開かないと、ログが出力されない場合があります。
- 一定期間後にsalesforceからログが取得できなくなることがあります(この場合、コンソールにdisconnectedの文字列が表示されます）。
- disconnectedが表示された場合、開発者コンソールを再表示するとログが出るようになります。（ログが取得できるようになると、コンソールにconnectedの文字列が表示されます）